<?php
    //nome da conta  e nome do pact
    namespace flavio\ProjetoTeste;
    //criação da classe 
    class Search{
        private $url = "https://viacep.com.br/ws/";//variavel que salva url
        
        //funcao q returna o endereço recebido via get e monta-o com as variaveis de acesso
        public function getAdressFromZipcode(string $zipCode):array{
            $zipCode= preg_replace('/[^0-9]/im','',$zipCode);

            $get = file_get_contents($this->url. $zipCode. "/json");

            return(array) json_decode($get);
        }
    }